<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', 'API\AuthController@register');
Route::post('/login', 'API\AuthController@login');
Route::post('/logout', 'API\AuthController@logout');
// Route::middleware('auth:api')->group(function () {
Route::group(['middleware'=> 'auth:api'], function () {

    Route::get('/get-user', 'API\AuthController@getUser');

    Route::post('/store', 'API\CategoryController@store');
    Route::get('/index', 'API\CategoryController@index');
    Route::get('/edit/{id}', 'API\CategoryController@edit');
    Route::post('/update/{id}', 'API\CategoryController@update');
    Route::post('/delete/{id}', 'API\CategoryController@delete');

    // Route::get('/create', 'API\CategoryController@create');

    // Route::get('/create', 'API\ProductController@create');
    Route::post('/product/store', 'API\ProductController@store');
    Route::get('/product/edit/{id}', 'API\ProductController@edit');
    Route::post('/product/update/{id}', 'API\ProductController@update');

    Route::post('/product/delete/{id}', 'API\ProductController@delete');

    Route::post('/cart/add', 'API\CartController@add');
    Route::post('/cart/list', 'API\CartController@list');
    Route::post('/cart/delete', 'API\CartController@delete');
    Route::post('/cart/update/{id}', 'API\CartController@update');
    Route::post('/cart/remove/{id}', 'API\CartController@remove');

    Route::post('/order', 'OrderController@store');
});
Route::get('/home', 'API\HomeController@index');
Route::get('/product/index', 'API\ProductController@index');
Route::get('/user/index', 'API\UserController@index');
Route::get('/user', 'API\UserController@loginuser');
