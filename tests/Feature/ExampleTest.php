<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Laravel\Passport\Passport;


class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testServerCreation()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $response = $this->post('/api/create-server');

        $response->assertStatus(201);
    }
}
