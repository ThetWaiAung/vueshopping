<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'user_id','total_products', 'total_quantity' ,'total_price'
    ];
    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function orderdetails()
    {
        return $this->hasMany('App\OrderDetail');
    }
}
