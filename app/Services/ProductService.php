<?php
namespace App\Services;

use App\Product;
use App\Category;
use Auth;
use Image;
use File;
use Illuminate\Support\Facades\Storage;

class ProductService
{
    public static function create($request)
    {   
        $fileName = Auth::user()->id .'_product'.time().'.jpg';
        $image = Image::make($request->image)->encode('jpg')->save(public_path("/img/". $fileName));
        $product = Product::create([
            "images" => $fileName,
            "prodescription" => $request->description,
            "name" => $request->name,
            "quantity" => $request->quantity,
            "price" => $request->price
        ]);

        $product->categories()->attach($request->chooses);
        return $product;
    }

    public static function update($request,$id)
    {
        $product = Product::whereId($id)->first();
 
        // if (preg_match('/(base64_|eval|system|shell_|exec|php_)/i',$request->image))
        // if image is have been in databases
        if($product->images != ''){
            //if image is have been in public folder
            if(File::exists(public_path('img/'.$product->images))){
                //if requested image is same as databases image
                if($product->images == $request->image){
                    $fileName = $product->images;
                }
                else{  //requested image is not same as databases image
                    unlink(public_path('img/' . $product->images));// deleted image from public folder

                    $fileName = $id.'_product'.time().'.jpg';
                    $image = Image::make($request->image)->encode('jpg')->save(public_path("/img/".$fileName));
                }
            }
            else{ // if image is have not been in public folder
                if($product->images != $request->image){ // if requested image is not same as database image
                    $fileName = $id.'_product'.time().'.jpg';
                    $image = Image::make($request->image)->encode('jpg')->save(public_path("/img/".$fileName));
                }
                else{ 
                    $fileName = '';
                }
            }       
        }
        else{ // image is not empty in database
               
            $fileName = $id.'_product'.time().'.jpg';
            $image = Image::make($request->image)->encode('jpg')->save(public_path("/img/".$fileName));
        }
        
        $product->update([
            "images" => $fileName,
            "prodescription" => $request->prodescription,
            "name" => $request->name,
            "quantity" => $request->quantity,
            "price" => $request->price
        ]);         

        $product->categories()->sync($request->chooses);    
        return $product;
    }
}
