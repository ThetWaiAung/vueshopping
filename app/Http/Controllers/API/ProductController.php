<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProductService;
use App\Http\Resources\ProductResource;
use App\Category;
use App\Product;
use Auth;
use Image;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $productpage = Product::paginate(10);
      
        return response()->json(['products'=>$products,'propage'=>$productpage]); 
    }
    public function create()
    {
        $categories = Category::all();
        return response()->json([
            'categories' => $categories
        ]);
    }

    public function store(Request $request)
    {   
        $this->validate($request, [
            'image' => 'required',
            'productdesc' => 'required',
            'name' => 'required',
            'productprice' => 'required|numeric|min:2',
            'productquantity' => 'required|numeric|min:2',
            'chooses' => 'required'
        ]);
        $product = ProductService::create($request);
        if($product){
            return response()->json('success', 200);
        }
    }
    public function edit($id)
    {
        $categories = Category::all();
        $editproduct = Product::find($id);
        $selectedcat = $editproduct->categories()->pluck('id')->toArray();
        return response()->json([
            'product'=> new ProductResource($editproduct),
            'categories'=>$categories,
            'chooses'=>$selectedcat
        ]);
    }
    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'image' => 'required',
            'prodescription' => 'required',
            'name' => 'required',
            'price' => 'required|numeric|min:2',
            'quantity' => 'required|numeric|min:2',
            'chooses' => 'required'
        ]);
        $product = ProductService::update($request,$id);
        return response()->json($product, 200);
    }
    public function delete($id) 
    {
        $product = Product::whereId($id)->first();
        $product->categories()->detach();
        $product->delete();        
        return response()->json('success',200);
    }
}
