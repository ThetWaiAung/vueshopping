<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $number = count($users);
        return response()->json(['users'=> $users,'number'=>$number]);
    }
    public function loginuser()
    {
        $user = Auth::user();
        return response()->json(['user'=> $user]);
    }
}
