<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    /**

        * Display a listing of the resource.

        *

        * @return \Illuminate\Http\Response

        */

    public function index()
    {
        $categories = Category::paginate(10);
        $allcat = Category::all();
        
        return response()->json(['categories'=>$categories,'allcat'=>$allcat]);
    }


    public function store(Request $req)
    {
        $this->validate(request(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        Category::create([
            'name' => request('name'),
            'description' => request('description'),
        ]);
        return response()->json(['status' => 201,
        'message' => 'Category is created']);
    }

    public function edit($id)
    {
        
        $category = Category::findOrFail($id);
        return response()->json($category);
    }

    public function update(Request $req, $id)
    {
        $this->validate(request(), [
            'name' => 'required',
            'description' => 'required'
        ]);

        $category = Category::find($id);
        $category->update([
            'name' => request('name'),
            'description' => request('description'),
        ]);
        // $d = Category::update($req->all())->where($id);
        // dd($d);
        return response()->json(['status' => 201,
        'message' => 'Category is created']);
    }
    public function delete($id) 
    {
        $categories = Category::find($id)->delete();
        return response()->json($categories);
    }

}
