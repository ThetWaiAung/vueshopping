<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Http\Resources\ProductResource;

class HomeController extends Controller
{
    public function index()
    {
        $products =	 Product::all();
     	
        return ProductResource::collection($products);    
    }
}
