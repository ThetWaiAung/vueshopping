<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Category;
use App\Product;

class CartController extends Controller
{
    public function add(Request $request)
    {
		// $id = Auth::user()->id;
		// dd($request->id);
    	$product = Product::find($request->id);

    	$cart = session()->get('cart');

    	if (! $cart) {
    		$cart = [
				$request->id => [
					'name' => $product->name,
					'price' => $product->price,
					'quantity'=> 1,
					'image' => 'img/'. $product->images
				]
			];
			session()->put('cart', $cart);
			return response()->json('success',200);

    	} elseif (isset($cart[$request->id])) {
    		$cart[$request->id]['quantity']++;
    		session()->put('cart',$cart);
    		return response()->json('success',200);
    	} else {
    		$cart[$request->id] = [
    			'name' => $product->name,
    			'price' => $product->price,
    			'quantity'=> 1,
    			'image' => 'img/'. $product->images
    		];
    		session()->put('cart',$cart);
    		return response()->json('success',200);
    	}
	}
	public function list(Request $request)
	{
		$cartlist = Session::get('cart');
		$quantity = 0;
        $price = 0;
        $products =0;
		if(isset($cartlist)){
			foreach($cartlist as $k=>$v){
				$quantity += $cartlist[$k]['quantity'];
			}
			foreach($cartlist as $k=>$v){
				$price += $cartlist[$k]['price']*$cartlist[$k]['quantity'];
			}

			$array = array_sort($cartlist);
		}
		else {
			$array = [];
        }
        $products = count($array);
		return response()->json(['list'=>$array,'totalquantity'=> $quantity,'totalprice'=>$price,'totalproduct'=> $products]);

	}
	public function delete()
	{
		if(Session::has('cart')){
			$cart = Session::forget('cart');
		}
		$cart = [];
		return response()->json($cart,200);
	}
	public function update(Request $request,$id)
	{

		$cart = Session::get('cart');

		$cart[$id]['quantity'] = $request->qvalue;

		session()->put('cart',$cart);
		return response()->json('success',200);
	}
	public function remove($id)
	{
		$cart = session()->get('cart');
		unset($cart[$id]);
		session()->put('cart',$cart);
		return response()->json('success',200);
	}
}
