<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use Session;
use Auth;


class OrderController extends Controller
{
    public function store(Request $request)
    {
        $user = Auth::user();

        $order = Order::create([
            'user_id' => $user->id,
            'total_products' => $request->totalproduct,
            'total_quantity' => $request->totalquantity,
            'total_price' => $request->totalprice
        ]);
        $cartlists = Session::get('cart');
        foreach($cartlists as $cartlist => $id){
            OrderDetail::create([
                "order_id" => $order->id,
                "product_name" => $id['name'],
                "quantity" => $id['quantity'],
                "price" => $id['price']
            ]);
        }
        return response()->json(['status'=>200]);
    }
}
