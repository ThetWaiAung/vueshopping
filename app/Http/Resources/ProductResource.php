<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'images' => $this->images,
            'image_url' => $this->images_url,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'prodescription' => $this->prodescription,
            'categories' => CategoryResource::collection($this->categories()->get())
        ];
    }
}
