<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'images','quantity', 'price','prodescription'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_products');
    }
    public function getImagesUrlAttribute()
    {
        return url( 'img/' . $this->images );
    }
}
