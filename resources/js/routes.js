import VueRouter from 'vue-router';
import MainLayout from './MainLayout.vue';
import Layout from './views/Layout.vue';
import Home from  './views/Home.vue';
import Checkout from  './views/Checkout.vue';
import Login from  './views/Login.vue';
import Register from  './views/Register.vue';
import Dashboard from  './views/Dashboard.vue';
import PermissionDenied from './views/PermissionDenied.vue';
import Category from './views/CreateCategory.vue';
import Product from './views/CreateProduct.vue';
import Tables from './views/Tables.vue';
import EditCategory from './views/EditCategory.vue';
import EditProduct from './views/EditProduct.vue';
import Products from './views/Products.vue';
import { userInfo } from 'os';

let routes = [
    {
        path: '/',
        // component: MainLayout,
        redirect: {
            name: 'home'
        }
        },
        // children:[
            {
                path: '/dashboard',
                name: 'dashboard',
                component: Dashboard,
                meta: { middlewareAuth: true , isAdmin : true },
                children: [

                    {
                        path: 'tables',
                        name: 'tables',
                        component: Tables,
                        meta: { middlewareAuth: true , isAdmin : true }
                    },
                    {
                        path: '/editcategory/:id',
                        name: 'editcategory',
                        component: EditCategory,
                        meta: { middlewareAuth: true , isAdmin : true }
                    },
                    {
                        path: 'products',
                        name: 'Products',
                        component: Products,
                        meta: { middlewareAuth: true , isAdmin : true }
                    },
                    {
                        path: '/editproduct/:id',
                        name: 'editproduct',
                        component: EditProduct,
                        meta: { middlewareAuth: true , isAdmin : true }
                    },
                    {
                        path: "403",
                        component: PermissionDenied
                    }
                ]
            },
            {
                path: '/',
                component: Layout,
                children:[
                    {
                        path: '/home',
                        name: 'home',
                        component: Home
                    },
                    {
                        path: 'checkout',
                        component: Checkout,
                        meta: { middlewareAuth: true }
                    },
                    {
                        path: 'login',
                        component: Login,
                    },
                    {
                        path: 'register',
                        component: Register,
                    },
                    {
                        path: "403",
                        component: PermissionDenied,
                    }
                ]
            }
    //     ]
    // }


];

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.middlewareAuth)) {
        if(!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });
        } else {
            if(to.matched.some(record => record.meta.isAdmin)) {
                if(auth.user.is_admin == to.meta.isAdmin) {
                    next();
                }else{
                    console.log(to);
                    next({
                        path: '403'
                    });
                }
            }else {
                next('/home');
            }
        }
    }
    next();
})
// router.beforeEach((to, from, next) => {
//     if(to.matched.some(record => record.meta.middlewareAuth) || to.matched.some(record => record.meta.isAdmin) ) {
//         if(!auth.check()) {
//             next({
//                 path: 'login',
//                 query: { redirect: to.fullPath }
//             });
//         } else {
            // if(to.matched.some(record => record.meta.isAdmin)) {
                // if(auth.user.is_admin == to.meta.isAdmin) {
                //     next();
                // }else{
                //     next({
                //         path: '403'
                //     });
                // }
            // }else {

            //     next();
            // }
//         }
//     }
//     next();
// })
export default router;
